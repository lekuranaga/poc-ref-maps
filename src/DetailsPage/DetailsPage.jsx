import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { IconButton } from '@material-ui/core';
import MenuBookRoundedIcon from '@material-ui/icons/MenuBookRounded';
import Pagination from '@material-ui/lab/Pagination';
import CircularProgress from '@material-ui/core/CircularProgress';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import MapPage from '../MapPage/MapPage';


const useStyles = makeStyles(() => ({
  root: {
    backgroundColor: '#03103B',
    width: '100vw',
    height: '100vh',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  searchContainer: {
    display: 'flex',
    justifyContent: 'center',
    padding: '20px 12px',
  },
  searchInput: {
    width: '100%',
    maxWidth: '500px',
    borderRadius: '50px',
  },
  loadingDiv: {
    marginTop: '30vh',
  },
  bookCard: {
    display: 'flex',
    flexDirection: 'column',
    border: '1px solid grey',
    borderRadius: '5px',
    padding: '25px',
    cursor: 'pointer',
    backfaceVisibility: 'hidden',
    transform: 'translateZ(0)',
    transition: 'transform 0.25s ease-out',
    color: '#707072',
    textAlign: 'center',
    height: '-webkit-fill-available',
    width: '-webkit-fill-available',
    fontSize: '14px',
    fontFamily: 'NexaLight',
    '&:hover': {
      transform: 'scale(1.05)',
    },
  },
  bookContainer: {
    width: '100%',
    margin: '0px',
  },
  bookButton: {
    color: '#f50057',
    fontSize: '50px',
  },
  paginationContainer: {
    padding: '20px 12px',
    display: 'flex',
    justifyContent: 'center',
  },
  ul: {
    '& .MuiPaginationItem-root': {
      color: '#fff',
    },
  },
  placeholder: {
    fontFamily: 'NexaRegular',
    marginLeft: '10px',
  },
}));

const DetailsPage = () => {
  const classes = useStyles();

  const [searchText, setSearchText] = useState('');
  const [books, setBooks] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [loading, setLoading] = useState(true);
  const [open, setOpen] = useState(false);
  const [loadingDetails, setLoadingDetails] = useState(false);
  const [bookToShow, setBookToShow] = useState({});

  const handleOpenDetails = (id) => {
    setLoadingDetails(true);
    setOpen(true);
    axios.get(`https://localhost:5001/api/v1/Livros/${id}`, {
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
    })
      .then((res) => {
        setBookToShow(res.data);
        setLoadingDetails(false);
      }).catch((err) => {
        console.log(err);
      });
  };

  const getAllBooks = () => {
    setLoading(true);
    axios.get('https://localhost:5001/api/v1/Livros', {
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
    })
      .then((res) => {
        setBooks(res.data.livros);
        setTotalPages(1);
        setLoading(false);
      }).catch((err) => {
        console.log(err);
      });
  };

  const handleSearch = (query, pageQuery) => {
    if (query === '') {
      getAllBooks();
      return;
    }
    setLoading(true);
    axios.get(`https://localhost:5001/api/v1/Livros/filtro?parametro=${query}&pagina=${pageQuery}`, {
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
    })
      .then((res) => {
        setBooks(res.data.livros);
        setTotalPages(Math.ceil(parseFloat(res.data.total) / 10));
        setLoading(false);
      }).catch((err) => {
        console.log(err);
      });
  };

  const handleChangePage = (event, value) => {
    setPage(value);
    handleSearch(searchText, value);
  };

  useEffect(() => {
    getAllBooks();
  }, []);

  return (
    <div className={classes.root}>
        <MapPage />
      <center>
        <IconButton>
          <MenuBookRoundedIcon className={classes.bookButton} />
        </IconButton>
      </center>
      <div className={classes.searchContainer}>
      </div>
      {loading ? <center className={classes.loadingDiv}><CircularProgress size={80} style={{ color: '#FFF' }} /></center>
        : (
          <>
            <Grid className={classes.bookContainer} container spacing={3}>
              {books.map((book) => (
                <Grid style={{ display: 'flex' }} item xs={6} sm={4} md={3} lg={2}>
                  <Paper
                    onClick={
                      () => handleOpenDetails(book.isbn13)
                    }
                    className={classes.bookCard}
                    elevation={3}
                  >
                    <LazyLoadImage
                      alt="Imagem do livro"
                      effect="black-and-white"
                      visibleByDefault
                      src={book.urlImagem}
                      placeholderSrc={book.urlImagem}
                    />
                    {book.titulo}
                  </Paper>
                </Grid>
              ))}
            </Grid>
            <Pagination className={classes.paginationContainer} classes={{ ul: classes.ul }} page={page} onChange={handleChangePage} count={totalPages} color="secondary" />
          </>
        )}
    </div>
  );
}

export default DetailsPage;