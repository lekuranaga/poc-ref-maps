import { Fragment } from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';

import './styles.css'

import book from '../assets/book-image.png';

import { Button } from '@material-ui/core';

const MapPage = () => {

    const position = [-23.1167885, -47.2262114];
    const position2 = [-23.1170874, -47.2241325];
    const position3 = [-23.1165147,-47.2237642];

    function Alerta() {
        alert('Troca efetuada com sucesso');
      }

    return (
        <Fragment>
            <MapContainer center={[-23.1167885, -47.2262114]} zoom={17}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={position}>
                    <Popup>
                        Ponto de Troca<br />
                        do seu zé
                        <br />
                        <img src={book} alt="Livro troca" />
                        <br />
                        <Button variant='contained' color='primary' onClick={Alerta}>Trocar</Button>
                    </Popup>
                </Marker>

                <Marker position={position2}>
                    <Popup>
                        Ponto de Troca<br />
                        do Leandro
                        <br />
                        <img src={book} alt="Livro troca" />
                        <br />
                        <Button variant='contained' color='primary' onClick={Alerta}>Trocar</Button>
                    </Popup>
                </Marker>

                <Marker position={position3}>
                    <Popup>
                        Ponto de Troca<br />
                        do num manju
                        <br />
                        <img src={book} alt="Livro troca" />
                        <br />
                        <Button variant='contained' color='primary' onClick={Alerta}>Trocar</Button>
                    </Popup>
                </Marker>
            </MapContainer>
        </Fragment>
    );
}

export default MapPage;